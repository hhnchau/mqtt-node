const mqtt = require('mqtt');

// Define the MQTT broker URL

const brokerUrl = {
    url: "mqtt://dingo.rmq.cloudamqp.com:1883",
    option: {
        qos: 0,
        username: "yljipchh:yljipchh",
        password: "VyPeqA0st8D3HbOUAUFUmkdA2lq73RoO",
    }
};

// Create an MQTT client
const client = mqtt.connect(brokerUrl.url, brokerUrl.option);

// Handle connection event
client.on('connect', () => {
  console.log('Connected to MQTT broker');

  // Subscribe to a topic
  const topic = 'test/topic';
  client.subscribe(topic, {qos: 1}, (err) => {
    if (err) {
      console.error(`Failed to subscribe to topic ${topic}:`, err);
    } else {
      console.log(`Subscribed to topic ${topic}`);
    }
  });
  // Publish a message to the topic
  const message = 'Hello MQTT';
  client.publish(topic, '', {retain: true, qos: 2}, (err) => {
    if (err) {
      console.error(`Failed to publish message:`, err);
    } else {
      console.log(`Message published: ${message}`);
    }
  });


    for (let i = 0; i < 15; i++) {
        client.publish(topic, `${i}`,{retain: true, qos: 2}, (err) => {
            if (err) {
            console.error(`Failed to publish message:`, err);
            } else {
            console.log(`Message published: ${i}`);
            }
        });
    }

});

// Handle message event
client.on('message', (topic, message) => {
    //demo(message.toString())
    //console.log(`Received message on topic ${topic}: ${message.toString()}`);
});

// Handle error event
client.on('error', (err) => {
  console.error('MQTT client error:', err);
});

// Handle close event
client.on('close', () => {
  console.log('MQTT client disconnected');
});

async function demo(y) {
    for (let i = 0; i < 15; i++) {
        console.log(`[${y}] Waiting ${i} seconds...`);
        await sleep(1000);
    }
    console.log('---Done---' , y);
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}