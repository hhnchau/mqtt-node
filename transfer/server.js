const express = require('express');
const mqtt = require('mqtt');

const app = express();
app.use(express.json());

const brokerUrl = {
    url: "mqtt://dingo.rmq.cloudamqp.com:1883",
    option: {
        qos: 0,
        username: "yljipchh:yljipchh",
        password: "VyPeqA0st8D3HbOUAUFUmkdA2lq73RoO",
    }
};

const mqttClient = mqtt.connect(brokerUrl.url, brokerUrl.option);

app.post('/api/:serial/send-task', async (req, res) => {
    const serial = req.params.serial;
    const requestId = `task_${Date.now()}`;
    const timeout = 5000;

    try {

            const taskPromise = new Promise((resolve, reject) => {
                mqttClient.publish(`raspberry_pi/AAA/task`, JSON.stringify({
                    ...req.body,
                    requestId
                }), (err) => {
                    if (err) {
                        reject(new Error('Error sending task'));
                    } else {
                        resolve('Task sent');
                    }
                });
            });

            mqttClient.subscribe(`raspberry_pi/AAA/task-response`, (err) => {
                if (err) {
                    console.error('Error subscribing to topic:', err);
                    res.status(500).json({ message: 'Internal Server Error' });
                }
            });

            const onMessage = (topic, message) => {
                if (topic === `raspberry_pi/AAA/task-response`) {
                    const response = JSON.parse(message.toString());
                    if (response.requestId === requestId) {
                        res.json(response);
                        mqttClient.removeListener('message', onMessage);
                    }
                }
            };

            mqttClient.on('message', onMessage);

            const timeoutId = setTimeout(() => {
                mqttClient.removeListener('message', onMessage);
                res.status(504).json({ message: 'Task Timeout' });
            }, timeout);

            taskPromise
                .then(message => {
                    clearTimeout(timeoutId);
                })
                .catch(error => {
                    clearTimeout(timeoutId);
                    console.error('Error sending task:', error);
                    res.status(500).json({ message: 'Internal Server Error' });
                });

    } catch (error) {
        console.error('Error sending task:', error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

/*
*
*/

app.get('/api/:serial/get-data', async (req, res) => {
    const serial = req.params.serial;
    const requestId = `getData_${Date.now()}`;
    const timeout = 5000;

    try {

            mqttClient.subscribe(`clever_link/${serial}/data`, (err) => {
                if (err) {
                    console.error('Error subscribing to topic:', err);
                    res.status(500).json({ message: 'Internal Server Error' });
                }
            });

            mqttClient.publish(`clever_link/${serial}`, JSON.stringify({ requestId }), (err) => {
                if (err) {
                    console.error('Error sending data request:', err);
                    res.status(500).json({ message: 'Internal Server Error' });
                }
            });

            const onMessage = (topic, message) => {
                if (topic === `clever_link/${serial}/data`) {
                    const response = JSON.parse(message.toString());

                    if (response.requestId === requestId) {
                        res.json(response.data);
                        mqttClient.removeListener('message', onMessage);
                    }
                }
            };

            mqttClient.on('message', onMessage);

            setTimeout(() => {
                mqttClient.removeListener('message', onMessage);
                res.status(504).json({ message: 'Request Timeout' });
            }, timeout);

    } catch (error) {
        console.error('Error getting data:', error);
        res.status(500).json({ message: 'Internal Server Error' });
    }
});

app.listen(8080, () => {
    console.log('API Gateway running on http://localhost:8080');
});