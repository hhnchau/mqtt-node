const mqtt = require('mqtt');

const brokerUrl = {
    url: "mqtt://dingo.rmq.cloudamqp.com:1883",
    option: {
        qos: 0,
        username: "yljipchh:yljipchh",
        password: "VyPeqA0st8D3HbOUAUFUmkdA2lq73RoO",
    }
};

const client = mqtt.connect(brokerUrl.url, brokerUrl.option);

client.on('connect', () => {
    client.subscribe(`clever_link/AAA`, (err) => {
        if (err) {
            console.error('Error subscribing to topic:', err);
        } else {
            console.log('Subscribed to task topic');
        }
    });
});

client.on('message', (topic, message) => {
    const request = JSON.parse(message.toString());
    if (topic.startsWith('raspberry_pi/') && request.requestId.endsWith('/task')) {

        handleTask(request)
            .then(result => {
                client.publish(`raspberry_pi/AAA/task-response`, JSON.stringify({
                    requestId: request.requestId,
                    result
                }), (err) => {
                    if (err) {
                        console.error('Error sending task response:', err);
                    }
                });
            })
            .catch(error => {
                console.error('Error handling task:', error);
            });
    } else if (topic.startsWith('clever_link') && request.requestId.startsWith('getData')) {

        const data = getData();

        client.publish(`clever_link/AAA/data`, JSON.stringify({
            requestId: request.requestId,
            data
        }), (err) => {
            if (err) {
                console.error('Error sending data:', err);
            }
        });
    }


});

async function handleTask(request) {
    return { success: true, message: 'Task completed' };
}

function getData() {
    return { temperature: 22.5, humidity: 60 };
}
